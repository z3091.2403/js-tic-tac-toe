
var CROSS = 'X';
var ZERO = 'O';
var EMPTY = ' ';
let player = CROSS;
let hasWinner = false;
let winnerLine = [];
let winnerSymbol = EMPTY;

let gameStorage = new Map(); 
startGame();

function startGame () {
  gameStorage.clear();
  renderGrid(3);
}

/* обработчик нажатия на клетку */
function cellClickHandler (row, col) {
  if (hasWinner === false) {
    if (gameStorage.get(`${row}${col}`) === undefined) {
      console.log(gameStorage)
      renderSymbolInCell(player, row, col)
      gameStorage.set(`${row}${col}`, player);
      player = player === CROSS ? ZERO: CROSS;
    }
    if (gameStorage.size > 4) {
      if (checkWinner()) {
        showWinner(winnerSymbol, winnerLine)
      };
    }
    if (gameStorage.size === 9 && hasWinner === false) {
      showMessage("Победила дружба");
    }
  }

  
  // Пиши код тут
  //console.log(`Clicked on cell: ${row}, ${col}`);

  /* Пользоваться методом для размещения символа в клетке так:
      renderSymbolInCell(ZERO, row, col);
   */
}

function showWinner (winnerSymbol, winnerLine) {
  showMessage(`Победили ${winnerSymbol === CROSS? `Крестики`: `Нолики`}`);
  for (const cell of winnerLine) {
    renderSymbolInCell(winnerSymbol, cell[0], cell[1],'#ff0000');
  }
}

function checkWinner () {
  for (let i = 0; i < 3; i++) {
    if ((gameStorage.get(`${i}0`) === gameStorage.get(`${i}1`) && gameStorage.get(`${i}0`) === gameStorage.get(`${i}2`)) && (gameStorage.get(`${i}0`) != undefined)) {
      winnerLine = [[i,0], [i,1], [i,2]];
      winnerSymbol = gameStorage.get(`${i}0`);
      hasWinner = true;
      return hasWinner; 
    } 
  }
    for (let j = 0; j < 3; j++) {
      if (gameStorage.get(`0${j}`) === gameStorage.get(`1${j}`) && gameStorage.get(`0${j}`) === gameStorage.get(`2${j}`) && (gameStorage.get(`0${j}`) != undefined)) {
        winnerLine = [[0,j], [1,j], [2,j]];
        winnerSymbol = gameStorage.get(`0${j}`);
        hasWinner = true;
        return hasWinner; 
        }
    }
    if (gameStorage.get(`00`) === gameStorage.get(`11`) && gameStorage.get(`00`) === gameStorage.get(`22`) && (gameStorage.get(`00`) != undefined)) {
      winnerLine = [[0,0], [1,1], [2,2]];
      winnerSymbol = gameStorage.get(`00`);
      hasWinner = true;
      return hasWinner; 

    }
    if (gameStorage.get(`02`) === gameStorage.get(`11`) && gameStorage.get(`02`) === gameStorage.get(`20`) && (gameStorage.get(`02`) != undefined)) {
      winnerLine = [[0,2], [1,1], [2,0]];
      winnerSymbol = gameStorage.get(`02`);
      hasWinner = true;
      return hasWinner; 
    }
}
/* обработчик нажатия на кнопку "Сначала" */
function resetClickHandler () {
  console.log('reset!');
  fillBoard(EMPTY, color = '#333');
  showMessage("");
  hasWinner = false;
  player = CROSS;
  startGame();
}

/* Служебные фукнции для взаимодействия с DOM. Данные функции нельзя редактировать! */
/* Показать сообщение */
function showMessage(text) {
  var msg = document.querySelector('.message');
  msg.innerText = text
}

/* Нарисовать игровое поле заданного размера */
function renderGrid (dimension) {
  var container = getContainer();
  container.innerHTML = '';

  for (let i = 0; i < dimension; i++) {
    var row = document.createElement('tr');
    for (let j = 0; j < dimension; j++) {
      var cell = document.createElement('td');
      cell.textContent = EMPTY;
      cell.addEventListener('click', () => cellClickHandler(i, j));
      row.appendChild(cell);
    }
    container.appendChild(row);
  }
}

/* Нарисовать символ symbol в ячейку(row, col) с цветом color */
function renderSymbolInCell (symbol, row, col, color = '#333') {
  var targetCell = findCell(row, col);

  targetCell.textContent = symbol;
  targetCell.style.color = color;
}

function fillBoard (symbol, color) {
  for (let i = 0; i < 3; i++) {
    for (let j = 0; j < 3; j++) {
      renderSymbolInCell(symbol, i, j, color);
    }
  }
}

function findCell (row, col) {
  var container = getContainer();
  var targetRow = container.querySelectorAll('tr')[row];
  return targetRow.querySelectorAll('td')[col];
}

function getContainer() {
  return document.getElementById('fieldWrapper');
}

/* Test Function */
/* Победа первого игрока */
function testWin () {
  clickOnCell(0, 2);
  clickOnCell(0, 0);
  clickOnCell(2, 0);
  clickOnCell(1, 1);
  clickOnCell(2, 2);
  clickOnCell(1, 2);
  clickOnCell(2, 1);
}

/* Ничья */
function testDraw () {
  clickOnCell(2, 0);
  clickOnCell(1, 0);
  clickOnCell(1, 1);
  clickOnCell(0, 0);
  clickOnCell(1, 2);
  clickOnCell(1, 2);
  clickOnCell(0, 2);
  clickOnCell(0, 1);
  clickOnCell(2, 1);
  clickOnCell(2, 2);
}

function clickOnCell (row, col) {
  findCell(row, col).click();
}
